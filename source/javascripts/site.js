const _second_ = 1000;
const _minute_ = _second_ * 60;
const _hour_ = _minute_ * 60;

var alarms = [];
var config = {};
var eveningInitialized = false;
var dayInitialized = true;
var visualAlarmInitialized = false;
var audioAlarmInitialized = false;
var audioPlaying = false;

$.get('/config.json').done(function(data) {
	config = data.config;
	config.day.start = alarmTime(data.config.day.start);
	config.day.end = alarmTime(data.config.day.end);
	config.alarm.visual = configTime(data.config.alarm.visual);
	config.alarm.audio = configTime(data.config.alarm.audio);
	config.alarm.timeout = configTime(data.config.alarm.timeout);
	$('audio').attr('src', config.alarm.sound);
}).fail(function(error) {
	console.log('Failed to fetch config :(');
	console.log(error);
});

function alarmTime(alarm) {
	if (alarm == null) {
		return -1;
	}

	const tokens = alarm.split(':');
	return tokens[0] * _hour_ + tokens[1] * _minute_;
}

function configTime(t) {
	return t * _second_;
}

$(document).ready(function() {
	updateAlarms();
	updateClock();

	setInterval(minuteTick, _minute_);
	setInterval(hourTick, _hour_);
	$('body').on('click', snooze);
	$('audio').on('pause', mobilePlaySingleShot);
});

function updateAlarms() {
	$.get('/alarms.json').done(function(data) {
		alarms = data.alarms;
		updateNextAlarm(true);
	}).fail(function(data){
		updateNextAlarm(false);
	});
}

function updateNextAlarm(success) {
	if (success == false) {
		$('#up_next').text('Failed to fetch alarms');
		return;
	}

	const currentAlarm = alarm(time(new Date()));
	if (currentAlarm) {
		$('#up_next').text(formatAlarm(currentAlarm));
	} else {
		$('#up_next').text('No alarm scheduled');
	}
}

function time(currentTime) {
	var currentHours = currentTime.getHours();
	const timeOfDay = (currentHours < 12) ? 'AM' : 'PM';
	currentHours = (currentHours > 12) ? currentHours - 12 : currentHours;
	currentHours = (currentHours == 0) ? 12 : currentHours;

	var currentMinutes = currentTime.getMinutes();
	currentMinutes = ((currentMinutes < 10) ? '0' : '') + currentMinutes;

	return {
		hours: currentHours,
		minutes: currentMinutes,
		timeOfDay: timeOfDay,
		dayOfWeek: currentTime.getDay(),
		date: currentTime
	};
}

function alarm(currentTime) {
	const now = currentTime.date.getHours() * _hour_ + currentTime.minutes * _minute_;
	if (alarms[currentTime.dayOfWeek]) {
		const alarmToday = alarmTime(alarms[alarmIndex(currentTime.dayOfWeek)]);

		if (now > alarmToday) {
			return alarms[alarmIndex(currentTime.dayOfWeek + 1)];
		}

		return alarms[alarmIndex(currentTime.dayOfWeek)];
	}

	if (currentTime.timeOfDay === 'PM') {
		return alarms[alarmIndex(currentTime.dayOfWeek + 1)];
	}

	return alarms[alarmIndex(currentTime.dayOfWeek)];
}

function alarmIndex(index) {
	if (index > 6) {
		return 0;
	}

	return index;
}

function formatAlarm(alarm) {
	if (alarm == null) {
		return null;
	}
	const tokens = alarm.split(':');
	const a = time(new Date(2000, 1, 1, tokens[0], tokens[1]));
	return a.hours + ':' + a.minutes + ' ' + a.timeOfDay;
}

function updateClock() {
	const currentTime = time(new Date());

	$('#day').text(formatDate(currentTime.date));
	$('#hour').text(currentTime.hours);
	$('#minute').text(currentTime.minutes);
	$('#am_pm').text(currentTime.timeOfDay);
}

function formatDate(date) {
	if (config.days == null || config.months == null) {
		return '';
	}

	const day = config.days[date.getDay()];
	const month = config.months[date.getMonth()];

	return day + ', ' + month + ' ' + date.getDate();
}

function minuteTick() {
	updateClock();

	const currentTime = time(new Date());
	if (isEvening(currentTime.date)) {
		initializeEvening();
	} else {
		initializeDay();
	}

	const currentAlarm = alarm(time(new Date()));
	initializeVisualAlarm(currentAlarm, currentTime);
	initializeAudioAlarm(currentAlarm, currentTime);
}

function isEvening(date) {
	return !isDay(date);
}

function isDay(date) {
	const currentTime = date.getHours() * _hour_ + date.getMinutes() * _minute_;
	return (currentTime >= config.day.start) && (currentTime < config.day.end);
}

function initializeEvening() {
	if (eveningInitialized) {
		return;
	}

	eveningInitialized = true;

	fadeBody(config.alarm.visual, config.opacity.night, function() {
		dayInitialized = false;
	});
}

function fadeBody(duration, opacity, callback) {
	$('body').fadeTo(duration, opacity, callback);
}

function initializeDay() {
	if (dayInitialized) {
		return;
	}

	dayInitialized = true;

	fadeBody(config.alarm.visual, config.opacity.day, function() {
		eveningInitialized = false;
	});
}

function initializeVisualAlarm(currentAlarm, currentTime) {
	if (visualAlarmInitialized) {
		return;
	}

	if (alarm == null) {
		return;
	}

	if (isVisualAlarm(currentAlarm, currentTime.date)) {
		visualAlarmInitialized = true;
		fadeBody(config.alarm.visual, config.opacity.day, function() {
			visualAlarmInitialized = false;
		});
	}
}

function isVisualAlarm(currentAlarm, currentDate) {
	const a = alarmTime(currentAlarm);
	const d = currentDate.getHours() * _hour_ + currentDate.getMinutes() * _minute_;
	const t = config.alarm.visual;
	return (a - d) == t;
}

function initializeAudioAlarm(currentAlarm, currentTime) {
	if (audioAlarmInitialized) {
		return;
	}

	if (alarm == null) {
		return;
	}

	if (isAudioAlarm(currentAlarm, currentTime.date)) {
		audioAlarmInitialized = true;
		play();
		alarmWatchDog = setTimeout(function() {
			snooze();
		}, config.alarm.timeout);
	}
}

function isAudioAlarm(currentAlarm, currentDate) {
	const a = alarmTime(currentAlarm);
	const d = currentDate.getHours() * _hour_ + currentDate.getMinutes() * _minute_;
	return (a - d) == 0;
}

function hourTick() {
	if (isEvening(new Date())) {
		// Don't poll for alarms changes over night
		return;
	}

	updateAlarms();
}

function snooze() {
	if (dayInitialized == false) {
		fadeBody('fast', config.opacity.day, function() {
			setTimeout(function() {
				fadeBody('fast', config.opacity.night);
			}, config.alarm.audio);
		});
	}

	stop();
	updateNextAlarm(true);
}

function stop() {
	const audio = audioPlayer();
	audio.pause();
	audioPlaying = false
	audio.currentTime = 0;
}

function audioPlayer() {
	return document.getElementById('audio');
}

function play() {
	const audio = audioPlayer();
	audioPlaying = true;
	audio.play();
}

function mobilePlaySingleShot() {
	const player = $('audio');
	player.removeAttr('controls');
	player.off('pause', mobilePlaySingleShot);
}
