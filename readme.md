### A very simple alarm clock.

#### Rational

I had a first generation apple watch.  About the only thing I used it for was as a bed side alarm.  Like the phone I could a number of different alarms but what I liked the most was that a simple tap of the watch face (I always had it charging in [nightstand][nightstand] mode) would wake the face up and show me the time and the next alarm.  Additionally, the face to fade in just before the audable alarm which I found to wake me more gently.

I parted ways with the watch and wanted to reclaim that very small use case.  I also happened to have a very old iPhone 3GS sitting on my desk that I was about to take to the dump.  I felt I could re-purpose the 3GS to be an alarm clock.

#### Execution

An incredibly simple [middleman][middleman] app.  It serves a single page of HTML along with 2 JSON based pages.  The JSON pages act as API end points the browser can query for:

1. User settings
1. Alarm times

The HTML page is really just a delivery system for the application itself.  A jquery based set of functions goes about tracking time, altering the display opacity, triggering audio playback and so on.

When the time reaches night the display will dim (the opacity will move towards 0).  Tapping the screen will briefly bring the opactiy back to 100% and after a short while the display will dim again.

To turn off the alarm simply tap the display.

#### Usage

1. Set your desired alarms in `data/alarms.yml`, one per day

1. Customize as required via `data/config.yml`

1. Deploy the docker container somewhere that your device can reach

1. Point the mobile browser to where you deployed to

1. Enjoy

#### Notes

1. Media will not auto play on an iOS based device.  Because you might be on your cell data plan the browser won't allow autonomous playback.  When you first load the page there will be audio controls present.  Press play to have a user initiated media request which will fetch the asset and start playing it.  When you pause the playback the controls will disappear.  From this point forward javascript can trigger play back.

1. If you refresh the page for some reason you will need to repeat the above.

1. Probably want to set the overall device brightness to the minimum

1. A setting of `null` for an alarm means there isn't to be an alarm today.

1. Alarms are set in 24-hour time

1. Alarm times are looked up every hour during the day.  If you need to make a change, simply update `data/alarms.yml` and re-deploy the site, the device will pick up the changes automatically.  You can also simply refresh the page if you really must get the update *right now*.


[nightstand]: https://support.apple.com/en-ca/HT205045
[middleman]: https://middlemanapp.com
